
public class Ford extends Vehiculo{

	public Ford(IMotor motor) {
		
		super(motor);
		
	}

	@Override
	public void mostrarCaracteristicas() {
		motor.consumirGasolina();
		motor.inyectarCombustible(10.10);
		
	}
	
	

}
