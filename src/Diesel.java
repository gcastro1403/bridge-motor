
public class Diesel implements IMotor{

	@Override
	public void inyectarCombustible(Double litros) {
		
		System.out.print("Se estan inyectando "+ litros + " de Diesel al motor");
		
	}

	@Override
	public void consumirGasolina() {
		
		System.out.println("Se esta consumiendo Diesel");
		
	}
	
	
}
