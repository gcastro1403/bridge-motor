
public interface IMotor {
	
	void inyectarCombustible(Double litros);
	
	void consumirGasolina();
	
}
